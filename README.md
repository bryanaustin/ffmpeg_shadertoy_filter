# ffmpeg_shadertoy_filter

Filter for using shadertoy code with ffmpeg.

Based on this three awesome previous projects:

- https://github.com/transitive-bullshit/ffmpeg-gl-transition
- https://github.com/nervous-systems/ffmpeg-opengl
- https://github.com/numberwolf/FFmpeg-Plus-OpenGL


Still a work in progress. Currently works with single-input, image-only shadertoys.

## Usage:

```
$ ffmpeg -hide_banner -h filter=shadertoy
Filter shadertoy
  Aplies a shadertoy using ffmpeg filter
    Inputs:
       #0: default (video)
    Outputs:
       #0: default (video)
shadertoy AVOptions:
  shadertoy_file    <string>     ..FV....... Required. Path to file containing a shadertoy's code
  vertex_file       <string>     ..FV....... Optional. Path to file with custom vertex shader for the shadertoy. By default this filter uses shadertoy webside default vertex shader.
  start             <duration>   ..FV....... Optional. Starting time for the shader render, in seconds. (default 0)
  duration          <duration>   ..FV....... Optional. Render duration (default 0)

This filter has support for timeline through the 'enable' option.
```

Just take the shadertoy code, put it in a local file, and run the filter like this:

`ffmpeg -i input_file -vf "shadertoy=path/to/file.glsl" -c:v your_codec_of_choice -f pick_a_format output_file`

It could also be used by ffplay, like this:

`ffplay input_file -vf "shadertoy=path/to/file.glsl"`


## Install instructions:

1.  Follow [ffmpeg download and building instructions](https://trac.ffmpeg.org/wiki/CompilationGuide)

    You **NEED** to compile ffmpeg. You **CAN'T** use this filter without compiling ffmpeg yourself.
    This mostly consist on cloning ffmpeg repo, installing dependencies, run `./configure`, and `make` inside repo directory.
    However, dependencies and configs may give you some work the first time, so take your time until you get it done.

2.  Clone this repo somewhere in your computer.

3.  Copy `vf_shadertoy.c` file to `[ffmpeg_repo]/libavfilter/`.

4.  Edit the file `[ffmpeg_repo]/libavfilter/allfilters.c`.

    Add the line `extern const AVFilter ff_vf_shadertoy;` after `extern const AVFilter ff_vf_zscale;`.
    The "after" positioning part is important so it won't break future ffmpeg updates (which you could do with just `git pull` inside ffmpeg repo directory).
    If you take a look at that list of filters, they're ordered alphabetically, and it's unlikely that a filter lower than "zscale" will appear any time soon.

5.  Edit the file `[ffmpeg_repo]/libavfilter/Makefile`.
    
    Add the line `OBJS-$(CONFIG_SHADERTOY_FILTER)              += vf_shadertoy.o` after `OBJS-$(CONFIG_ZSCALE_FILTER)                 += vf_zscale.o`
    Same positioning logic as before.

6.  Install GLFW and GLEW, both for production and development use cases.

    In Ubuntu 20.04, this is the command: `sudo apt install libglfw3-dev libglfw3 libglew2.1 libglew-dev`
    Other distros may have other commands, package names, and so on. You'll gotta check that out.

7.  Re-configure ffmpeg, adding the following flags: 
    `--enable-opengl --extra-libs='-lGLEW -lEGL -lglfw' --enable-filter=shadertoy`

8.  Build again ffmpeg with `make`.

    With this done, you should have a newer ffmpeg executable file inside ffmpeg repo directory.
    If you also want to replace your distro's ffmpeg executable with your own version, you could do `sudo make install`.


## Notes:

- So far, this filter works wonderful with effects shadertoys (like night vision, vcr artifacts, and stuff like that).
- Some shadertoys need minor tweaking because otherwise the output get vertically flipped.
- Many (of the coolest) shadertoys use multiple inputs and/or multiple buffers. The filter don't have that yet. I want to add the ability to have multiple video inputs, but have little time for it, and no ETA. Second in priority is the ability to use multiple buffers, and last in line is audio.
- Shadertoy website uses default noises that we don't provide here. So you may have to get your hands dirty to obtain the same results from some shadertoys, specially the ones that consist in amazing animations. So try at first rendering with different video files, and eventually just go for lavfi wizardry.
- Some shadertoys may just plain not work, and you'll have to understand their code in order to fix them. *C'est la vie*.

## Licence:

I do libre software, so IYAM just use it. But consider this software:

- Has been made using bits of code from other previous software. Mostly the repos mentioned before, and shadertoy website.
- Shadertoys code may each one have their own licensing.
- IDK what's the deal with FFMPEG license when it's about filters.
- The same goes for the GL libs.

So, frankly, I have no idea. But if it depends on me, then the license I would choose is GPLv3.


## Disclaimer about compatibility:

I'm a GNU/Linux user, and this works on GNU/Linux.

I'm neither user, friend, enthusiast, evangelist, customer, partner, employee, or anything other than enemy of Microsoft and their products, including Windows. So I could not care less about how to make this work on that system. Consider this repo explicitly hostile towards Microsoft. Same goes for Apple stuff: I don't use that, I see no reason at all to use that, and I see that stuff hurting people's rights all around the world, so IMHO should be avoided like a disease. If you want this working on those systems, just fork it and do with the code as you please.

